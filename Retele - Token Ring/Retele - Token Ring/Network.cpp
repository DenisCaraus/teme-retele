#include "Network.h"



Network::Network(int pcNumber)
{
	for (int index = 0; index < pcNumber; ++index)
	{
		string temp = "192.168.0." + to_string(index);
		listIP.emplace_back(temp);
	}
	resetTempData();
}


Network::~Network()
{
}

void Network::run()
{
	int count = 0;
	string temp = "";
	while (count < listIP.size())
	{
		token.printHistory();

		if ((token.isFree()) && (sourceIP == ""))
		{
			if (!menu())
				break;
			system("pause");
		}

		// check if current computer wants to send info
		if (listIP[count] == sourceIP)
		{
			if (token.setToken(sourceIP, destinationIP, message));
			{
				system("pause");
				resetTempData();
			}
		}

		temp = token.passThrough(listIP[count]);

		if (temp != "")
		{
			cout << "Message: " << temp << endl;
			system("pause");
		}

		if (++count == listIP.size())
			count = 0;
	}
}

bool Network::checkInput(const string& input)
{
	if (find(listIP.begin(), listIP.end(), input) == listIP.end())
		return false;
	return true;
}

bool Network::menu()
{
	printIP();

	int choice;
	cout << " 1. Continue \n 2. Introduce message \n 3. Exit" << endl;
	cin >> choice;

	switch (choice)
	{
	case 1: break;
	case 2:
	{
		while (!checkInput(sourceIP))
		{
			cout << "Source IP: ";
			cin >> sourceIP;
		}
		while (!checkInput(destinationIP))
		{
			cout << "Destination IP: ";
			cin >> destinationIP;
		}
		cout << "Message: ";
		cin.ignore();
		getline(cin, message);
		break;
	}
	case 3: return false;
	}
	return true;
}

void Network::resetTempData()
{
	sourceIP = "";
	destinationIP = "";
	message = "";
}

void Network::printIP()
{
	cout << "IP List:" << endl;
	copy(listIP.begin(), listIP.end(), std::ostream_iterator<string>(std::cout, "\n"));
	cout << endl << endl;
}
