#pragma once
#include<Algorithm>
#include"Token.h"
class Network
{
public:
	Network(int pcNumber);
	~Network();

	void run();
	bool checkInput(const string& input);
	bool menu();
	void resetTempData();
	void printIP();

private:
	string sourceIP, destinationIP, message;
	vector<string> listIP;
	Token token;
};

