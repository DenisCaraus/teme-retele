#include "Token.h"



Token::Token()
{
	resetToken();
}


Token::~Token()
{
}

void Token::resetToken()
{
	history.erase(history.begin(), history.end());
	message = "";
	sourceIP = "";
	destinationIP = "";
	free = true;
	reachedDestination = false;
}

bool Token::setToken(string source, string destination, string message)
{
	if (!free)
		return false;

	sourceIP = source;
	destinationIP = destination;
	this->message = message;
	free = false;

	cout << "SourceIP: " << source << "\n DestinationIP: " << destination << endl;

	return true;
}

bool Token::changeMessage(string source, string message)
{
	if(source != sourceIP)
		return false;

	this->message = message;

	return true;
}

bool Token::hasReachedDestination()
{
	return reachedDestination;
}

bool Token::isFree()
{
	return free;
}

bool Token::setFree(string source)
{
	if (source != sourceIP)
		return false;

	resetToken();

	cout << endl << "Token freed!" << endl;

	return true;
}

void Token::printHistory()
{
	cout << endl << endl << "History: " << endl;

	if (history.empty())
		cout << "none" << endl;
	else
		copy(history.begin(), history.end(), std::ostream_iterator<string>(std::cout, "\n"));

	cout << endl;

	return;
}

string Token::passThrough(string IP)
{
	if(!isFree())
		history.emplace_back(IP);
	string temp = "";
	if (IP == destinationIP)
	{
		cout << endl << "Message received!" << endl;
		reachedDestination = true;
		temp = message;
	}

	if (IP == sourceIP)
		if (hasReachedDestination())
			setFree(IP);

	return temp;
}
