#pragma once
#include<string>
#include<vector>
#include<iostream>
#include<iterator>

using namespace std;

class Token
{
public:
	Token();
	~Token();

	void resetToken();
	bool setToken(string source, string destination, string message);
	bool changeMessage(string source, string message);
	bool hasReachedDestination();
	bool isFree();
	bool setFree(string source);
	void printHistory();
	string passThrough(string IP);

private:
	string sourceIP, destinationIP, message;
	bool reachedDestination, free;
	vector<string> history;
};

