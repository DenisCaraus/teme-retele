#include<iostream>
#include<string>

using namespace std;

bool HasCorrectFormat(const string& input)
{
	for (char character : input)
	{
		if ((character != '0') && (character != '1'))
			return false;
	}
	return true;
}

void RemoveUnnecessaryZeros(string& input)
{
	while (1)
	{
		if (input[0] != '0')
			return;
		input.erase(0, 1);
	}
}

void AddZerosAtEnd(string& input, uint8_t nrZeros)
{
	for (int index = 0; index < nrZeros - 1; ++index)
	{
		input.insert(input.end(), '0');
	}
}

char XOR(char first, char second)
{
	if (first == second)
		return '0';
	else
		return '1';
}

string CalculateRemainder(string first, const string& second)
{
	while (first.length() >= second.length())
	{
		uint8_t counter = 0;
		for (char character : second)
		{
			first[counter] = XOR(first[counter], character);
			counter++;
		}
		RemoveUnnecessaryZeros(first);
	}

	return first;
}

string GetMessageSent(string message, const string& remainder)
{
	int counter = remainder.length();
	for (char character : remainder)
	{
		message[message.length() - counter] = character;
		counter--;
	}

	return message;
}

bool IsSentCorrectly(string message, const string& polynom)
{
	if (CalculateRemainder(message, polynom).length() == 0)
		return true;
	return false;
}

// returns remainder
string CRC(string& message, const string& polynom)
{
	AddZerosAtEnd(message, polynom.length());
	string remainder = CalculateRemainder(message, polynom);
	message = GetMessageSent(message, remainder);

	return remainder;
}

void read(string& input, const string& message)
{
	while (true)
	{
		cout << message;
		cin >> input;
		if (HasCorrectFormat(input))
			break;
		else
			cout << "Date introduse incorect! Incearca din nou!" << endl;
	}
}

string ParityByte(string message)
{
	string endByte = "0000000";
	uint8_t counter = 0, numberExtraBits = 0;
	for (int index = 0; index < message.length(); ++index)
	{
		if (message[index] == '1')
		{
			counter++;
			endByte[(index - numberExtraBits) % 7] = XOR(message[index], endByte[(index - numberExtraBits) % 7]);
		}
		if ((index != 0) || (message.length() == 1))
		{
			if (((index + 1 - numberExtraBits) % 7 == 0) || (index == message.length() - 1))
			{
				numberExtraBits++;
				if (counter % 2 == 0)
					message.insert(index + 1, "0");
				else
					message.insert(index+ 1, "1");
				index++;
				counter = 0;
			}
		}
	}
	counter = 0;
	for (char character : endByte)
	{
		if (character == '1')
		{
			counter++;
		}
	}

	if (counter % 2 == 0)
		endByte.append("0");
	else
		endByte.append("1");


	return message + endByte;
}

int main()
{
	string message, polynom, transported, remainder, userTransport;

	read(message, "M(x) : ");
	read(polynom, "C(x) : ");
	read(userTransport, "U(x) : ");

	RemoveUnnecessaryZeros(message);
	RemoveUnnecessaryZeros(polynom);
	RemoveUnnecessaryZeros(userTransport);

	transported = message;

	remainder = CRC(transported, polynom);

	system("cls");
	cout << "M(x) : " << message << endl
		<< "C(x) : " << polynom << endl
		<< "T(x) : " << transported << endl;

	if(remainder.length() == 0)
		cout << "R(x) : " << '0' << endl;
	else
		cout << "R(x) : " << remainder << endl;

	if (IsSentCorrectly(userTransport, polynom))
		cout << "U(x) : " << userTransport << " a fost trimis corect!" << endl;
	else
		cout << "U(x) : " << userTransport << " nu a fost trimis corect!" << endl;

	cout << "Mesaj trimis prin metoda bitului de paritate: " << ParityByte(message) << std::endl;

	return 0;
}