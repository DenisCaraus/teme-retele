#include "Computer.h"

Computer::Computer(int packetCount, int windowSize): packetCount(packetCount), windowSize(windowSize)
{
	startingPacket = 0;
}

Computer::~Computer()
{
}

int Computer::getWindowSize()
{
	return windowSize;
}

int Computer::getPacketCount()
{
	return packetCount;
}

std::vector<int> Computer::getPackets()
{
	return packets;
}

void Computer::setWindowSize(int windowSize)
{
	this->windowSize = windowSize;
}

void Computer::setPacketCount(int packetCount)
{
	this->packetCount = packetCount;
}

int Computer::getStartingPacket()
{
	return startingPacket;
}
