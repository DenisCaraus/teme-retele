#pragma once
#include<vector>

class Computer
{
public:
	Computer(int packetCount = 0, int windowSize = 0);
	~Computer();
	int getWindowSize();
	int getPacketCount();
	std::vector<int> getPackets();
	void setWindowSize(int windowSize);
	void setPacketCount(int packetCount);
	int getStartingPacket();

protected:
	std::vector<int> packets;
	int windowSize;
	int packetCount;
	int startingPacket;
};

