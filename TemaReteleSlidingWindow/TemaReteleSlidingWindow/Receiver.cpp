#include "Receiver.h"


Receiver::Receiver(int packetCount, int windowSize) : Computer(packetCount, windowSize)
{
	receivedPackages.resize(packetCount);
}

std::vector<int> Receiver::receivePackets(std::vector<int> packetsReceived)
{
	std::vector<int> ack;
	int packetCheck = startingPacket;
	int errorPos = -1;

	if (packetsReceived.size() == 0)
	{
		ack.push_back(-1);
		return ack;
	}

	for (int packet : packetsReceived)
	{
		if (packetCheck >= packetCount)
			break;

		while (receivedPackages[packetCheck] == true)
			packetCheck++;

		if (packetCheck != packet)
		{
			errorPos = packetCheck;
			ack.push_back(-1);
			packetCheck++;
		}
		packets.push_back(packet);
		ack.push_back(packet);
		receivedPackages[packet] = true;
		packetCheck++;
	}

	int index = 0;
	while(ack.size() + index < windowSize)
	{
		if (receivedPackages[index + ack.size()] == true)
		{
			index++; 
			continue;
		}

		if (errorPos == -1)
			errorPos = packets.size() + ack.size() - 1;

		ack.push_back(-1);
	}

	if (errorPos == -1)
	{
		startingPacket += packetsReceived.size();
	}
	else
	{
		startingPacket = errorPos;
	}

	std::sort(packets.begin(), packets.end());
	return ack;
}

bool Receiver::hasReceivedAll()
{
	return (packets.size() == packetCount);
}

Receiver::~Receiver()
{
}
