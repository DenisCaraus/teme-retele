#pragma once
#include "Computer.h"
#include <algorithm>

class Receiver : public Computer
{
public:
	Receiver(int packetCount = 0, int windowSize = 0);
	~Receiver();
	std::vector<int> receivePackets(std::vector<int> packetsReceived);
	bool hasReceivedAll();

private:
	std::vector<bool> receivedPackages;
};
