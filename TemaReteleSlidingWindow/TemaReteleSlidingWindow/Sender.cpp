#include "Sender.h"



Sender::Sender(int packetCount, int windowSize) : Computer(packetCount, windowSize)
{
	for (int index = 0; index < packetCount; ++index)
	{
		packets.push_back(index);
	}
	sentPackages.resize(packetCount);
}

std::vector<int> Sender::sendPackets(int errorPacket)
{
	std::vector<int> sent;
	for(int index = startingPacket; index < startingPacket + windowSize; ++index)
	{
		if (index == errorPacket)
			continue;
		if (index >= packetCount)
			break;
		if (sentPackages[index] == true)
			continue;

		sent.push_back(packets[index]);
	}
	return sent;
}

bool Sender::receiveAcknowledgements(std::vector<int> ack)
{
	bool isCorrect = true;
	int currentPacket = startingPacket;
	for (int ackPacket : ack)
	{
		while (sentPackages[currentPacket] == true)
		{
			startingPacket++;
			currentPacket++;
		}

		if (ackPacket == currentPacket)
		{
			sentPackages[currentPacket] = true;

			if (isCorrect)
			{
				startingPacket++;
				currentPacket = startingPacket;
				continue;
			}
		}
		else
		{
			isCorrect = false;
		}
		currentPacket++;
	}

	if (currentPacket < packetCount)
	{
		while (sentPackages[currentPacket] == true)
		{
			startingPacket++;
			currentPacket++;
			if (currentPacket == packetCount)
			{
				break;
			}
		}
	}

	return isCorrect;
}

Sender::~Sender()
{
}
