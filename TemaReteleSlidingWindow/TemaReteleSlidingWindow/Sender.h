#pragma once
#include "Computer.h"

class Sender : public Computer
{
public:
	Sender(int packetCount = 0, int windowSize = 0);
	~Sender();
	std::vector<int> sendPackets(int errorPacket = 0);
	bool receiveAcknowledgements(std::vector<int> ack);

private:
	std::vector<bool> sentPackages;
};

