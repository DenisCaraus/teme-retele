#include"Sender.h"
#include"Receiver.h"
#include<iostream>
#include<iterator>

void reader(int& packetCount, int& windowSize, int& errorPackage)
{
	while (true)
	{
		std::cout << "Introduceti numarul de pachete: " << std::endl;
		std::cin >> packetCount;
		if (packetCount > 0)
			break;
		std::cout << "Numarul de pachete trebuie sa fie mai mare ca 0! " << std::endl;
	}

	while (true)
	{
		std::cout << "Introduceti marimea ferestrei: " << std::endl;
		std::cin >> windowSize;
		if ((windowSize > 0) && (windowSize <= packetCount))
			break;
		std::cout << "Marimea trebuie sa fie mai mare ca 0 si mai mica sau egala cu numarul de pachete! " << std::endl;
	}

	std::cout << "Introduceti pachetul pierdut (daca este mai mare ca nr de pachete sau mai mic ca 0 nu se va pierde niciun pachet): " << std::endl;
	std::cin >> errorPackage;
}

void slidingWindow(int packetCount, int windowSize, int errorPackage)
{
	bool receivedStatus = false;
	std::vector<int> tempVector;
	Sender sender(packetCount, windowSize);
	Receiver receiver(packetCount, windowSize);

	while (!receivedStatus)
	{
		tempVector = sender.sendPackets(errorPackage);

		std::cout << "Pachete trimise: " << std::endl;
		std::copy(tempVector.begin(), tempVector.end(), std::ostream_iterator<int>(std::cout, ", "));
		std::cout << std::endl << std::endl;

		tempVector = receiver.receivePackets(tempVector);

		std::cout << "Raspunsuri trimise: " << std::endl;
		std::copy(tempVector.begin(), tempVector.end(), std::ostream_iterator<int>(std::cout, ", "));
		std::cout << std::endl << std::endl;

		if (sender.receiveAcknowledgements(tempVector))
			std::cout << "Toate pachetele au fost trimise cu succes" << std::endl;
		else
		{
			std::cout << "Unele pachete nu au putut fi trimise!" << std::endl;
			errorPackage = -1;
		}

		tempVector = receiver.getPackets();

		std::cout << "Toate pachetele primite pana acum: " << std::endl;
		std::copy(tempVector.begin(), tempVector.end(), std::ostream_iterator<int>(std::cout, ", "));
		std::cout << std::endl << std::endl;

		receivedStatus = receiver.hasReceivedAll();
	}

	tempVector = sender.getPackets();

	std::cout << "Toate pachetele posibile: " << std::endl;
	std::copy(tempVector.begin(), tempVector.end(), std::ostream_iterator<int>(std::cout, ", "));
	std::cout << std::endl << std::endl;

	tempVector = receiver.getPackets();

	std::cout << "Toate pachetele primite: " << std::endl;
	std::copy(tempVector.begin(), tempVector.end(), std::ostream_iterator<int>(std::cout, ", "));
	std::cout << std::endl << std::endl;
}

int main()
{
	int packetCount, windowSize, errorPackage;

	reader(packetCount, windowSize, errorPackage);
	slidingWindow(packetCount, windowSize, errorPackage);

	return 0;
}